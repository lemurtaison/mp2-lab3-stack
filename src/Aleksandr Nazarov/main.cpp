// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// Copyright (c) Гергель В.П. 28.07.2000
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Динамические структуры данных - тестирование стека

#include <iostream>
#include "TStack.h"

using namespace std;

void main()
{
  TStack<int> st(2);
  int temp;
  cout << "Support programm to stack" << endl;
  for (int i = 0; i < 35; i++)
  {
    st.Put(i);
    cout << "Put value " << i << " code " << st.GetRetCode() << endl;
  }
  while (!st.IsEmpty())
  {
    temp = st.Get();
    cout << "Get value " << temp << " code " << st.GetRetCode() << endl;
  }
}
